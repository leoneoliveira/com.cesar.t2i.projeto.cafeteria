package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.AulaJHipsterApp;

import com.mycompany.myapp.domain.PedidoProduto;
import com.mycompany.myapp.repository.PedidoProdutoRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PedidoProdutoResource REST controller.
 *
 * @see PedidoProdutoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AulaJHipsterApp.class)
public class PedidoProdutoResourceIntTest {

    private static final Integer DEFAULT_CDPRODUTO_PEDIDO = 1;
    private static final Integer UPDATED_CDPRODUTO_PEDIDO = 2;

    private static final String DEFAULT_TAMANHO = "AAAAAAAAAA";
    private static final String UPDATED_TAMANHO = "BBBBBBBBBB";

    private static final Float DEFAULT_PRECO = 1F;
    private static final Float UPDATED_PRECO = 2F;

    private static final String DEFAULT_OBS = "AAAAAAAAAA";
    private static final String UPDATED_OBS = "BBBBBBBBBB";

    @Autowired
    private PedidoProdutoRepository pedidoProdutoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPedidoProdutoMockMvc;

    private PedidoProduto pedidoProduto;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PedidoProdutoResource pedidoProdutoResource = new PedidoProdutoResource(pedidoProdutoRepository);
        this.restPedidoProdutoMockMvc = MockMvcBuilders.standaloneSetup(pedidoProdutoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PedidoProduto createEntity(EntityManager em) {
        PedidoProduto pedidoProduto = new PedidoProduto()
            .cdprodutoPedido(DEFAULT_CDPRODUTO_PEDIDO)
            .tamanho(DEFAULT_TAMANHO)
            .preco(DEFAULT_PRECO)
            .obs(DEFAULT_OBS);
        return pedidoProduto;
    }

    @Before
    public void initTest() {
        pedidoProduto = createEntity(em);
    }

    @Test
    @Transactional
    public void createPedidoProduto() throws Exception {
        int databaseSizeBeforeCreate = pedidoProdutoRepository.findAll().size();

        // Create the PedidoProduto
        restPedidoProdutoMockMvc.perform(post("/api/pedido-produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pedidoProduto)))
            .andExpect(status().isCreated());

        // Validate the PedidoProduto in the database
        List<PedidoProduto> pedidoProdutoList = pedidoProdutoRepository.findAll();
        assertThat(pedidoProdutoList).hasSize(databaseSizeBeforeCreate + 1);
        PedidoProduto testPedidoProduto = pedidoProdutoList.get(pedidoProdutoList.size() - 1);
        assertThat(testPedidoProduto.getCdprodutoPedido()).isEqualTo(DEFAULT_CDPRODUTO_PEDIDO);
        assertThat(testPedidoProduto.getTamanho()).isEqualTo(DEFAULT_TAMANHO);
        assertThat(testPedidoProduto.getPreco()).isEqualTo(DEFAULT_PRECO);
        assertThat(testPedidoProduto.getObs()).isEqualTo(DEFAULT_OBS);
    }

    @Test
    @Transactional
    public void createPedidoProdutoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pedidoProdutoRepository.findAll().size();

        // Create the PedidoProduto with an existing ID
        pedidoProduto.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPedidoProdutoMockMvc.perform(post("/api/pedido-produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pedidoProduto)))
            .andExpect(status().isBadRequest());

        // Validate the PedidoProduto in the database
        List<PedidoProduto> pedidoProdutoList = pedidoProdutoRepository.findAll();
        assertThat(pedidoProdutoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPedidoProdutos() throws Exception {
        // Initialize the database
        pedidoProdutoRepository.saveAndFlush(pedidoProduto);

        // Get all the pedidoProdutoList
        restPedidoProdutoMockMvc.perform(get("/api/pedido-produtos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pedidoProduto.getId().intValue())))
            .andExpect(jsonPath("$.[*].cdprodutoPedido").value(hasItem(DEFAULT_CDPRODUTO_PEDIDO)))
            .andExpect(jsonPath("$.[*].tamanho").value(hasItem(DEFAULT_TAMANHO.toString())))
            .andExpect(jsonPath("$.[*].preco").value(hasItem(DEFAULT_PRECO.doubleValue())))
            .andExpect(jsonPath("$.[*].obs").value(hasItem(DEFAULT_OBS.toString())));
    }

    @Test
    @Transactional
    public void getPedidoProduto() throws Exception {
        // Initialize the database
        pedidoProdutoRepository.saveAndFlush(pedidoProduto);

        // Get the pedidoProduto
        restPedidoProdutoMockMvc.perform(get("/api/pedido-produtos/{id}", pedidoProduto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pedidoProduto.getId().intValue()))
            .andExpect(jsonPath("$.cdprodutoPedido").value(DEFAULT_CDPRODUTO_PEDIDO))
            .andExpect(jsonPath("$.tamanho").value(DEFAULT_TAMANHO.toString()))
            .andExpect(jsonPath("$.preco").value(DEFAULT_PRECO.doubleValue()))
            .andExpect(jsonPath("$.obs").value(DEFAULT_OBS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPedidoProduto() throws Exception {
        // Get the pedidoProduto
        restPedidoProdutoMockMvc.perform(get("/api/pedido-produtos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePedidoProduto() throws Exception {
        // Initialize the database
        pedidoProdutoRepository.saveAndFlush(pedidoProduto);
        int databaseSizeBeforeUpdate = pedidoProdutoRepository.findAll().size();

        // Update the pedidoProduto
        PedidoProduto updatedPedidoProduto = pedidoProdutoRepository.findOne(pedidoProduto.getId());
        updatedPedidoProduto
            .cdprodutoPedido(UPDATED_CDPRODUTO_PEDIDO)
            .tamanho(UPDATED_TAMANHO)
            .preco(UPDATED_PRECO)
            .obs(UPDATED_OBS);

        restPedidoProdutoMockMvc.perform(put("/api/pedido-produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPedidoProduto)))
            .andExpect(status().isOk());

        // Validate the PedidoProduto in the database
        List<PedidoProduto> pedidoProdutoList = pedidoProdutoRepository.findAll();
        assertThat(pedidoProdutoList).hasSize(databaseSizeBeforeUpdate);
        PedidoProduto testPedidoProduto = pedidoProdutoList.get(pedidoProdutoList.size() - 1);
        assertThat(testPedidoProduto.getCdprodutoPedido()).isEqualTo(UPDATED_CDPRODUTO_PEDIDO);
        assertThat(testPedidoProduto.getTamanho()).isEqualTo(UPDATED_TAMANHO);
        assertThat(testPedidoProduto.getPreco()).isEqualTo(UPDATED_PRECO);
        assertThat(testPedidoProduto.getObs()).isEqualTo(UPDATED_OBS);
    }

    @Test
    @Transactional
    public void updateNonExistingPedidoProduto() throws Exception {
        int databaseSizeBeforeUpdate = pedidoProdutoRepository.findAll().size();

        // Create the PedidoProduto

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPedidoProdutoMockMvc.perform(put("/api/pedido-produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pedidoProduto)))
            .andExpect(status().isCreated());

        // Validate the PedidoProduto in the database
        List<PedidoProduto> pedidoProdutoList = pedidoProdutoRepository.findAll();
        assertThat(pedidoProdutoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePedidoProduto() throws Exception {
        // Initialize the database
        pedidoProdutoRepository.saveAndFlush(pedidoProduto);
        int databaseSizeBeforeDelete = pedidoProdutoRepository.findAll().size();

        // Get the pedidoProduto
        restPedidoProdutoMockMvc.perform(delete("/api/pedido-produtos/{id}", pedidoProduto.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PedidoProduto> pedidoProdutoList = pedidoProdutoRepository.findAll();
        assertThat(pedidoProdutoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PedidoProduto.class);
        PedidoProduto pedidoProduto1 = new PedidoProduto();
        pedidoProduto1.setId(1L);
        PedidoProduto pedidoProduto2 = new PedidoProduto();
        pedidoProduto2.setId(pedidoProduto1.getId());
        assertThat(pedidoProduto1).isEqualTo(pedidoProduto2);
        pedidoProduto2.setId(2L);
        assertThat(pedidoProduto1).isNotEqualTo(pedidoProduto2);
        pedidoProduto1.setId(null);
        assertThat(pedidoProduto1).isNotEqualTo(pedidoProduto2);
    }
}
