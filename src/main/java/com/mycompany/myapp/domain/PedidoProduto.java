package com.mycompany.myapp.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PedidoProduto.
 */
@Entity
@Table(name = "pedido_produto")
public class PedidoProduto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cdproduto_pedido")
    private Integer cdprodutoPedido;

    @Column(name = "tamanho")
    private String tamanho;

    @Column(name = "preco")
    private Float preco;

    @Column(name = "obs")
    private String obs;

    @ManyToOne
    private Pedido pedido;

    @ManyToOne
    private Produto cdPedido;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCdprodutoPedido() {
        return cdprodutoPedido;
    }

    public PedidoProduto cdprodutoPedido(Integer cdprodutoPedido) {
        this.cdprodutoPedido = cdprodutoPedido;
        return this;
    }

    public void setCdprodutoPedido(Integer cdprodutoPedido) {
        this.cdprodutoPedido = cdprodutoPedido;
    }

    public String getTamanho() {
        return tamanho;
    }

    public PedidoProduto tamanho(String tamanho) {
        this.tamanho = tamanho;
        return this;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public Float getPreco() {
        return preco;
    }

    public PedidoProduto preco(Float preco) {
        this.preco = preco;
        return this;
    }

    public void setPreco(Float preco) {
        this.preco = preco;
    }

    public String getObs() {
        return obs;
    }

    public PedidoProduto obs(String obs) {
        this.obs = obs;
        return this;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public PedidoProduto pedido(Pedido pedido) {
        this.pedido = pedido;
        return this;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Produto getCdPedido() {
        return cdPedido;
    }

    public PedidoProduto cdPedido(Produto produto) {
        this.cdPedido = produto;
        return this;
    }

    public void setCdPedido(Produto produto) {
        this.cdPedido = produto;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PedidoProduto pedidoProduto = (PedidoProduto) o;
        if (pedidoProduto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pedidoProduto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PedidoProduto{" +
            "id=" + getId() +
            ", cdprodutoPedido='" + getCdprodutoPedido() + "'" +
            ", tamanho='" + getTamanho() + "'" +
            ", preco='" + getPreco() + "'" +
            ", obs='" + getObs() + "'" +
            "}";
    }
}
