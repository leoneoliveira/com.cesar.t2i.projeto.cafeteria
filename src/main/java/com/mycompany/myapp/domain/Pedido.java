package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Pedido.
 */
@Entity
@Table(name = "pedido")
public class Pedido implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cd_pedido")
    private Integer cdPedido;

    @Column(name = "dtpedido")
    private LocalDate dtpedido;

    @OneToMany(mappedBy = "pedido")
    @JsonIgnore
    private Set<PedidoProduto> cdPedidos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCdPedido() {
        return cdPedido;
    }

    public Pedido cdPedido(Integer cdPedido) {
        this.cdPedido = cdPedido;
        return this;
    }

    public void setCdPedido(Integer cdPedido) {
        this.cdPedido = cdPedido;
    }

    public LocalDate getDtpedido() {
        return dtpedido;
    }

    public Pedido dtpedido(LocalDate dtpedido) {
        this.dtpedido = dtpedido;
        return this;
    }

    public void setDtpedido(LocalDate dtpedido) {
        this.dtpedido = dtpedido;
    }

    public Set<PedidoProduto> getCdPedidos() {
        return cdPedidos;
    }

    public Pedido cdPedidos(Set<PedidoProduto> pedidoProdutos) {
        this.cdPedidos = pedidoProdutos;
        return this;
    }

    public Pedido addCdPedido(PedidoProduto pedidoProduto) {
        this.cdPedidos.add(pedidoProduto);
        pedidoProduto.setPedido(this);
        return this;
    }

    public Pedido removeCdPedido(PedidoProduto pedidoProduto) {
        this.cdPedidos.remove(pedidoProduto);
        pedidoProduto.setPedido(null);
        return this;
    }

    public void setCdPedidos(Set<PedidoProduto> pedidoProdutos) {
        this.cdPedidos = pedidoProdutos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pedido pedido = (Pedido) o;
        if (pedido.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pedido.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pedido{" +
            "id=" + getId() +
            ", cdPedido='" + getCdPedido() + "'" +
            ", dtpedido='" + getDtpedido() + "'" +
            "}";
    }
}
