import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AulaJHipsterPedidoModule } from './pedido/pedido.module';
import { AulaJHipsterPedidoProdutoModule } from './pedido-produto/pedido-produto.module';
import { AulaJHipsterProdutoModule } from './produto/produto.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        AulaJHipsterPedidoModule,
        AulaJHipsterPedidoProdutoModule,
        AulaJHipsterProdutoModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AulaJHipsterEntityModule {}
