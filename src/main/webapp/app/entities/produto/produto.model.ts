import { BaseEntity } from './../../shared';

export class Produto implements BaseEntity {
    constructor(
        public id?: number,
        public cdProduto?: number,
        public nomeproduto?: string,
    ) {
    }
}
