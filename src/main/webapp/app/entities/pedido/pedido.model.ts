import { BaseEntity } from './../../shared';

export class Pedido implements BaseEntity {
    constructor(
        public id?: number,
        public cdPedido?: number,
        public dtpedido?: any,
        public cdPedidos?: BaseEntity[],
    ) {
    }
}
