import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AulaJHipsterSharedModule } from '../../shared';
import {
    PedidoProdutoService,
    PedidoProdutoPopupService,
    PedidoProdutoComponent,
    PedidoProdutoDetailComponent,
    PedidoProdutoDialogComponent,
    PedidoProdutoPopupComponent,
    PedidoProdutoDeletePopupComponent,
    PedidoProdutoDeleteDialogComponent,
    pedidoProdutoRoute,
    pedidoProdutoPopupRoute,
    PedidoProdutoResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...pedidoProdutoRoute,
    ...pedidoProdutoPopupRoute,
];

@NgModule({
    imports: [
        AulaJHipsterSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PedidoProdutoComponent,
        PedidoProdutoDetailComponent,
        PedidoProdutoDialogComponent,
        PedidoProdutoDeleteDialogComponent,
        PedidoProdutoPopupComponent,
        PedidoProdutoDeletePopupComponent,
    ],
    entryComponents: [
        PedidoProdutoComponent,
        PedidoProdutoDialogComponent,
        PedidoProdutoPopupComponent,
        PedidoProdutoDeleteDialogComponent,
        PedidoProdutoDeletePopupComponent,
    ],
    providers: [
        PedidoProdutoService,
        PedidoProdutoPopupService,
        PedidoProdutoResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AulaJHipsterPedidoProdutoModule {}
