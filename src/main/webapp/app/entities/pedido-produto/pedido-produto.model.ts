import { BaseEntity } from './../../shared';

export class PedidoProduto implements BaseEntity {
    constructor(
        public id?: number,
        public cdprodutoPedido?: number,
        public tamanho?: string,
        public preco?: number,
        public obs?: string,
        public pedido?: BaseEntity,
        public cdPedido?: BaseEntity,
    ) {
    }
}
